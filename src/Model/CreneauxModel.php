<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauxModel extends AbstractModel{

    protected static $table = 'creneau';

    protected $id;
    protected $salle;
    protected $start_at;
    protected $nbrhours;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrhours()
    {
        return $this->nbrhours;
    }


    public static function insert($post){
        App::getDatabase()->prepareInsert(
            "INSERT INTO ". self::$table . " (id_salle, start_at, nbrhours) VALUES (?,?,?) ",
            [$post['salle'], $post['start_at'], $post['nbrhours']]
        );
    }

    public static function getCreneauxWithData()
    {
        return App::getDatabase()->prepare(
            "SELECT  c.id, c.id_salle , c.start_at , c.nbrhours,  s.title as title FROM " . self::$table . " AS c 
                LEFT JOIN salle AS s ON c.id_salle = s.id",
            [], get_called_class()
        );
    }


}