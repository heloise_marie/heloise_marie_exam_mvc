<?php

namespace App\Controller;

use App\Model\CreneauxModel;
use App\Model\SallesModel;
use App\Model\UsersModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $users = UsersModel::all();
        $salles = SallesModel::all();
        $creneaux = CreneauxModel::getCreneauxWithData();
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'users' => $users,
            'salles' => $salles,
            'creneaux' =>$creneaux
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
