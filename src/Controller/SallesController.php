<?php

namespace App\Controller;

use App\Model\SallesModel;
use App\Service\Form;
use App\Service\Validation;

class SallesController extends BaseController{

    public function add(){

        $errors= array();

        if(!empty($_POST['submitted'])) {

            $post = $this->cleanXss($_POST);
            $validation = new  Validation();
            $errors['title'] = $validation->textValid($post['title'],'title', 2, 70);
           if(strlen(strval($post['maxuser'])) > 11){
               $errors['maxuser'] = 'Valeur trop importante';
           }
            if ($validation->IsValid($errors)){
                SallesModel::insert($post);
                $this->redirect('home');
            }
        }
        $form = new Form($errors);
        $this->render('app.salles.add',array(
            'form'=> $form,

        ));
    }

}