<?php

namespace App\Controller;


use App\Model\CreneauxModel;
use App\Model\SallesModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauxController extends BaseController {

    public function add(){
        $errors = array();
        $salle = SallesModel::all();

        if(!empty($_POST['submitted'])) {

            $post = $this->cleanXss($_POST);
            $validation = new  Validation();
            if (strlen(strval($post['nbrhours'])) > 11) {
                $errors['nbrhours'] = 'Valeur trop importante';
            }
            if ($validation->IsValid($errors)) {
                CreneauxModel::insert($post);
                $this->redirect('home');
            }
        }
        $this->dump($_POST);
        $form =new Form($errors);
        $this->render('app.creneaux.add',array(
            'form'=>$form,
            'salle'=>$salle
        ));
    }
    public function single(){
        $creneaux = CreneauxModel::getCreneauxWithData();
        $this->render('app.creneaux.single', array(
            'creneaux' => $creneaux
        ));
    }
}