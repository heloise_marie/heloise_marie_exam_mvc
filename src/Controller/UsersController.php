<?php

namespace App\Controller;

use App\Model\UsersModel;
use App\Service\Form;
use App\Service\Validation;

class UsersController extends BaseController{


    public function add(){
        $errors =array();

        if(!empty($_POST['submitted'])){

            $post = $this->cleanXss($_POST);
            $validation = new Validation();
            $errors['nom'] = $validation->textValid($post['nom'],'nom', 2, 70);
            $errors['email'] = $validation->emailValid($post['email']);

            if ($validation->IsValid($errors)){
                UsersModel::insert($post);
                $this->redirect('home');
            }

        }
        $form = new Form($errors);
        $this->render('app.users.add',array(
            'form'=> $form,

        ));

    }
}