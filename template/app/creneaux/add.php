<h1>Ajout Créneau</h1>

<form action="" method="post">
    <?php
    echo $form->label('salle', 'Salle');
    echo $form->selectEntity('salle', $salle, 'title');
    echo $form->error('salle');

    echo $form->label('start_at', 'Date de commencement');
    echo $form->input('start_at', 'datetime-local');
    echo $form->error('start_at');

    echo $form->label('nbrhours', 'Durée (en heures)');
    echo $form->input('nbrhours', 'number');
    echo $form->error('nbrhours');

    echo $form->submit('submitted');

    ?>

</form>