<table>
    <thead>
    <tr>
        <th>Nom</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user){?>
        <tr>
            <td><?php echo $user->getNom() ?></td>
            <td><?php echo $user->getEmail() ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th>Nom de salle</th>
        <th>Capacité maximale</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($salles as $salle){?>
        <tr>
            <td><?php echo $salle->getTitle() ?></td>
            <td><?php echo $salle->getMaxuser() ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th>Nom de salle</th>
        <th>Date de commencement</th>
        <th>Durée</th>
        <th>Détails</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($creneaux as $creneau){?>
        <tr>
            <td><?php echo $creneau->title ?></td>
            <td><?php echo $creneau->getStartAt() ?></td>
            <td><?php echo $creneau->getNbrhours() ?></td>
            <td><a href="<?= $view->path('details-creneaux',  array('id'=> $creneau->getId())); ?>">Voir plus</a></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
